namespace EmployeeApi.Models
{
    public class Employee
    {
        public DateTimeOffset CreatedAt { get; set; } 
        public string? Name { get; set; }
        public string? Avatar { get; set; }
        public int Id { get; set; }
    }
}
