using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DealManagementSystem.Models; 

[Route("api/[controller]")]
[ApiController]
public class DealsController : ControllerBase
{
    private readonly AppDbContext _context;

    public DealsController(AppDbContext context)
    {
        _context = context;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Deal>>> GetDeals()
    {
        return await _context.Deals.Include(d => d.Hotel).ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Deal>> GetDeal(int id)
    {
        var deal = await _context.Deals.Include(d => d.Hotel).FirstOrDefaultAsync(d => d.Id == id);

        if (deal == null)
        {
            return NotFound();
        }

        return deal;
    }

    [HttpPost]
    public async Task<ActionResult<Deal>> PostDeal(Deal deal)
    {
        _context.Deals.Add(deal);
        await _context.SaveChangesAsync();

        return CreatedAtAction(nameof(GetDeal), new { id = deal.Id }, deal);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutDeal(int id, Deal deal)
    {
        if (id != deal.Id)
        {
            return BadRequest("Deal ID does not match.");
        }

        _context.Entry(deal).State = EntityState.Modified;

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!_context.Deals.Any(e => e.Id == id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteDeal(int id)
    {
        var deal = await _context.Deals.FindAsync(id);
        if (deal == null)
        {
            return NotFound();
        }

        _context.Deals.Remove(deal);
        await _context.SaveChangesAsync();

        return NoContent();
    }
}
