using EmployeeApi.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.IO;
using System.Linq;

namespace EmployeeApi.Services
{
    public class EmployeeService
    {
        private readonly HttpClient _httpClient;

        public EmployeeService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Employee[]> GetEmployeesAsync()
        {
            var response = await _httpClient.GetAsync("https://6603f95c2393662c31d0463e.mockapi.io/api/employee");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadFromJsonAsync<Employee[]>();
        }

        private string JsonFileName => Path.Combine("Data", "employees.json");

        public IEnumerable<Employee> GetEmployees()
        {
            using (var jsonFileReader = File.OpenText(JsonFileName))
            {
                return JsonConvert.DeserializeObject<Employee[]>(jsonFileReader.ReadToEnd());
            }
        }

        public void SaveEmployees(IEnumerable<Employee> employees)
        {
            using (var outputStream = File.CreateText(JsonFileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(outputStream, employees);
            }
        }

        public void AddEmployee(Employee employee)
        {
            var employees = GetEmployees().ToList();
            employees.Add(employee);
            SaveEmployees(employees);
        }

        public void UpdateEmployee(Employee employee)
        {
            var employees = GetEmployees().ToList();
            var index = employees.FindIndex(emp => emp.Id == employee.Id);
            if (index != -1)
            {
                employees[index] = employee;
                SaveEmployees(employees);
            }
        }

        public void DeleteEmployee(int id)
        {
            var employees = GetEmployees().ToList();
            var employee = employees.FirstOrDefault(emp => emp.Id == id);
            if (employee != null)
            {
                employees.Remove(employee);
                SaveEmployees(employees);
            }
        }
    }
}
